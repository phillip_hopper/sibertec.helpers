<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;

use Closure;
use Exception;
use stdClass;
use Symfony\Component\Yaml\Yaml;


class MiscFunctions
{
    public static function DebugPrint($message)
    {
        if (!defined('DEBUG'))
            return;

        print $message . PHP_EOL;
    }

    /**
     *
     * @param string $url
     * @param string $data 'name1=value1&name2=value2'
     * @return string
     */
    public static function DoPostRequest($url, $data)
    {
        $context_options = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );

        $context = stream_context_create($context_options);

        $fp = @fopen($url, 'r', false, $context);  // may just need 'r' and not 'rb'

        if (!$fp) {
            //throw new Exception("Problem with $url");
            echo "Problem with $url";
            return false;
        }

        $response = stream_get_contents($fp);

        if ($response !== false)
            return $response;

        //throw new Exception("Problem reading data from $url");
        echo "Problem reading data from $url";
        return false;
    }

    public static function AjaxStringToArray($str)
    {
        $rows = explode("\r\n", $str);

        if (count($rows) < 2)
            return null;

        // first row is array keys
        $keys = explode("\t", $rows[0]);
        $returnVal = array();

        // loop through remaining rows
        for ($i = 1; $i < count($rows); $i++) {

            if (strlen($rows[$i])) {
                $row = array();
                $vals = explode("\t", $rows[$i]);

                // loop through columns
                for ($j = 0; $j < count($keys); $j++) {
                    $row[$keys[$j]] = $vals[$j];
                }

                $returnVal[] = $row;
            }
        }

        return $returnVal;
    }

    /**
     * Searches the include path for a file
     * @param string $file
     * @return bool
     */
    public static function FileExistsInPath($file)
    {
        $ps = explode(PATH_SEPARATOR, get_include_path());
        foreach ($ps as $path) {
            if (file_exists($path . DIRECTORY_SEPARATOR . $file))
                return true;
        }

        return file_exists($file);
    }

    /**
     * Searches the include path for a file, returning the full path and filename if found
     * @param string $file
     * @return mixed Full path and filename if found, otherwise FALSE
     */
    public static function GetFilenameInPath($file)
    {
        $ps = explode(PATH_SEPARATOR, get_include_path());
        foreach ($ps as $path) {
            $fullname = realpath($path . DIRECTORY_SEPARATOR . $file);
            if ($fullname !== false)
                return $fullname;
        }

        return false;
    }

    /**
     * Executes a php file and returns the results
     * @param string $file
     * @return string
     */
    public static function EvalPhpFile($file)
    {
        if (!is_file($file))
            return '';

        ob_start();
        /** @noinspection PhpIncludeInspection */
        include $file;
        return ob_get_clean();
    }

    /**
     * Removes all files and subdirectories from a directory. Optionally delete the directory also.
     * @param string $dirName
     * @param bool $deleteDirectoryAlso Delete the directory after emptying it.
     */
    public static function ClearDirectory($dirName, $deleteDirectoryAlso)
    {
        $structure = glob(rtrim($dirName, "/") . '/*');
        if (is_array($structure)) {
            foreach ($structure as $file) {
                if (is_dir($file)) {
                    self::ClearDirectory($file, true);
                } elseif (is_file($file)) {
                    unlink($file);
                }
            }
        }
        if ($deleteDirectoryAlso) {
            rmdir($dirName);
        }
    }

    /**
     * @param array ...$files
     */
    public static function IncludeOnceMulti(...$files)
    {
        foreach($files as $file)
            /** @noinspection PhpIncludeInspection */
            include_once $file;
    }

    /**
     * After completing $function, send the response to the user to release the browser, then continue processing
     *
     * @param Closure $function
     */
    public static function SendAndContinue($function)
    {
        ignore_user_abort(true);
        set_time_limit(0);

        ob_start();

        $function();

        header('Connection: close');
        header('Content-Length: '.ob_get_length());
        ob_end_flush();
        ob_flush();
        flush();
    }

    /**
     * SOURCE: https://stackoverflow.com/a/25370978
     * @param string $size A value from php.ini like 2K or 4M
     *
     * @return float
     */
    public static function ParseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }

    /**
     * SOURCE: https://stackoverflow.com/a/25370978
     * @return float|int
     */
    public static function GetMaxUploadFileSize()
    {
        $max_size = 0;

        // Start with post_max_size.
        $post_max_size = self::ParseSize(ini_get('post_max_size'));
        if ($post_max_size > 0)
            $max_size = $post_max_size;

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = self::ParseSize(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size)
            $max_size = $upload_max;

        return $max_size;
    }

    /**
     * @param $file_name
     * @param bool $assoc
     *
     * @return array|stdClass
     * @throws Exception
     */
    public static function YamlDecodeFile($file_name, $assoc=true)
    {
        $array = Yaml::parseFile($file_name);

        if ($assoc)
            return $array;

        return self::ArrayToObject($array);
    }

    /**
     * @param array $array
     *
     * @return stdClass
     */
    public static function ArrayToObject($array)
    {
        // Create new stdClass object
        $object = new stdClass();

        // Use loop to convert array into stdClass object
        foreach ($array as $key => $value) {
            if (is_array($value))
                $value = self::ArrayToObject($value);

            $object->$key = $value;
        }

        return $object;
    }
}
