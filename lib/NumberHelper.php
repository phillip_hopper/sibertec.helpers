<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;


class NumberHelper
{
    /**
     * @param int $value
     *
     * @return string
     */
    public static function InchesToFeetInches($value)
    {
        if (empty($value)) {
            return "N/A";
        } else {
            $inches = $value % 12;
            $feet = ($value - $inches) / 12;
            return $feet . '\' ' . $inches . '"';
        }
    }

    /**
     * @param $string
     *
     * @return float
     */
    public static function ParseCurrency($string)
    {
        if (empty($string))
            return (float)0;

        $s = str_replace('$', '', $string);
        $s = str_replace(',', '', $s);
        $s = str_replace(' ', '', $s);
        $s = str_replace('(', '-', $s);
        $s = str_replace(')', '', $s);

        return (float)$s;
    }

    /**
     * @param $string
     *
     * @return int
     */
    public static function ParseInt($string)
    {
        if (empty($string))
            return (int)0;

        $s = str_replace(',', '', $string);
        $s = str_replace(' ', '', $s);
        $s = str_replace('(', '-', $s);
        $s = str_replace(')', '', $s);

        return (int)$s;
    }

    /**
     * @param $float
     * @param int $decimals
     *
     * @return float
     */
    public static function RoundCurrency($float, $decimals = 2)
    {
        return (float)number_format($float, $decimals, '.', '');
    }

    /**
     * @param $float
     * @param int $decimals
     * @param bool $include_symbol
     * @param bool $negative_parens
     *
     * @return string
     */
    public static function FormatCurrency($float, $decimals = 2, $include_symbol = true, $negative_parens = false)
    {
        $float = floatval($float);

        $is_neg = ($float < 0);

        $s = number_format($float, $decimals, '.', ',');

        if ($include_symbol) {

            $s = '$' . $s;

            if ($is_neg) {
                $s = str_replace('-', '', $s);

                if ($negative_parens)
                    $s = "($s)";
                else$s = "-$s";
            }
        }

        return $s;
    }
}
