<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;


class Request
{
    /**
     * Allows the filter_input to be overridden for unit testing
     * @var string
     */
    public static $FilterCallable = 'filter_input';

    /** @var  string[] */
    private static $url_parts;

    /** @var  string[] */
    private static $put_vars = null;

    /** @var  string[] */
    private static $delete_vars = null;


    /**
     * Doing it this way so we can mock the input values
     * @param $type
     * @param $variable_name
     * @param int $filter
     * @param null $options
     * @return mixed
     */
    private static function filterInput($type, $variable_name, $filter=FILTER_DEFAULT, $options=null)
    {
        return call_user_func(self::$FilterCallable ,$type, $variable_name, $filter, $options);
    }


    /**
     * Returns the $_SERVER[$variable_name] value as a string
     *
     * @param string $variable_name
     *
     * @return string
     */
    public static function IsServerSet($variable_name)
    {
        return self::filterInput(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_SERVER[$variable_name] value as a string
     *
     * @param string $variable_name
     * @param string $default
     *
     * @return string
     */
    public static function ServerStr($variable_name, $default = '')
    {
        $val = (string)self::filterInput(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW);
        return self::TrimString($val, $default);
    }

    private static function TrimString($val, $default)
    {
        if (empty($val))
            return $default;

        // do not remove new line or tab - default is " \t\n\r\0\x0B"
        $val = trim((string)$val, " \0\x0B");
        if (empty($val))
            return $default;

        return $val;
    }

    /**
     * Returns true if the $_POST[$variable_name] is set
     *
     * @param string $variable_name
     *
     * @return bool
     */
    public static function IsPostSet($variable_name)
    {
        return self::filterInput(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_POST[$variable_name] value as a string
     *
     * @param string $variable_name
     * @param string $default
     *
     * @return string
     */
    public static function PostStr($variable_name, $default = '')
    {
        $val = (string)self::filterInput(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW);
        return self::TrimString($val, $default);
    }

    /**
     * Returns the $_POST[$variable_name] value as an int
     *
     * @param string $variable_name
     *
     * @return int
     */
    public static function PostInt($variable_name)
    {
        return (int) self::filterInput(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Returns the $_POST[$variable_name] value as a boolean.
     * Empty, zero and 'false' are false, otherwise true
     *
     * @param string $variable_name
     *
     * @return boolean
     */
    public static function PostBool($variable_name)
    {
        $val = strtolower(self::PostStr($variable_name));
        switch ($val) {
            case '':
            case '0':
            case 'false':
                return false;

            default:
                return true;
        }
    }

    /**
     *
     * @param string $variable_name
     * @param float $default
     * @param int $decimal_places
     * @return float
     */
    public static function PostFloat($variable_name, $default = 0.0, $decimal_places = null)
    {
        $val = self::filterInput(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        if (empty($val)) $val = $default;

        if ($decimal_places != null) $val = number_format($val, $decimal_places);

        return (float) $val;
    }

    /**
     *
     * @param string $variable_name
     * @return int a timestamp on success, <b>null</b> otherwise.
     */
    public static function PostDate($variable_name)
    {
        $val = self::PostStr($variable_name);

        if (empty($val)) return null;
        return strtotime($val);
    }

    /**
     * Returns true if the $_GET[$variable_name] is set
     * @param string $variable_name
     * @return bool
     */
    public static function IsGetSet($variable_name)
    {
        return self::filterInput(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_GET[$variable_name] value as a string
     *
     * @param string $variable_name
     * @param string $default
     *
     * @return string
     */
    public static function GetStr($variable_name, $default = '')
    {
        $val = (string)self::filterInput(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW);
        return self::TrimString($val, $default);
    }

    /**
     * Returns the $_GET[$variable_name] value as an int
     * @param string $variable_name
     * @return int
     */
    public static function GetInt($variable_name)
    {
        return (int) self::filterInput(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Returns the $_GET[$variable_name] value as a boolean.
     * Empty, zero and 'false' are false, otherwise true
     *
     * @param string $variable_name
     *
     * @return boolean
     */
    public static function GetBool($variable_name)
    {
        $val = strtolower(self::GetStr($variable_name));
        switch ($val) {
            case '':
            case '0':
            case 'false':
                return false;

            default:
                return true;
        }
    }

    /**
     * Returns the $_GET[$variable_name] value as a float
     * @param string $variable_name
     * @param float $default
     * @param int $decimal_places
     * @return float
     */
    public static function GetFloat($variable_name, $default = 0.0, $decimal_places = null)
    {

        $val = self::filterInput(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT);

        if (empty($val)) $val = $default;

        if ($decimal_places != null) $val = number_format($val, $decimal_places);

        return (float) $val;
    }

    /**
     * First looks for the value in $_POST, then $_GET if not found
     *
     * @param $variable_name
     * @param null $default
     *
     * @return string|null
     */
    public static function PostGetStr($variable_name, $default = null)
    {
        if (self::IsPostSet($variable_name)) {
            return self::PostStr($variable_name);
        }

        if (self::IsGetSet($variable_name)) {
            return self::GetStr($variable_name);
        }

        return $default;
    }

    /**
     * First looks for the value in $_POST, then $_GET if not found
     *
     * @param $variable_name
     * @param null $default
     *
     * @return int|null
     */
    public static function PostGetInt($variable_name, $default = null)
    {
        if (self::IsPostSet($variable_name)) {
            return self::PostInt($variable_name);
        }

        if (self::IsGetSet($variable_name)) {
            return self::GetInt($variable_name);
        }

        return $default;
    }

    /**
     * First looks for the value in $_POST, then $_GET if not found
     *
     * @param $variable_name
     * @param bool $default
     *
     * @return bool
     */
    public static function PostGetBool($variable_name, $default = false)
    {
        if (self::IsPostSet($variable_name)) {
            return self::PostBool($variable_name);
        }

        if (self::IsGetSet($variable_name)) {
            return self::GetBool($variable_name);
        }

        return $default;
    }

    /**
     * Returns an array of strings from the POST collection whose keys all begin with $prefix
     *
     * @param string $prefix
     *
     * @return string[]
     */
    public static function PostPrefixStr($prefix)
    {
        $return_val = array();

        $keys = array_keys($_POST);
        foreach ($keys as $key) {
            if (StringHelper::BeginsWith($key, $prefix)) {
                $return_val[] = self::PostStr($key);
            }
        }

        return $return_val;
    }

    /**
     * Returns the URL as an array of segments split on the slashes
     * @return array|string[]
     */
    public static function UrlParts()
    {
        if (empty(self::$url_parts)) {

            $uri = strtolower(self::ServerStr('REQUEST_URI'));

            // remove the query string
            $pos = strpos($uri, '?');
            if ($pos !== false) {
                $uri = substr($uri, 0, $pos);
            }

            self::$url_parts = preg_split('@/@', $uri, NULL, PREG_SPLIT_NO_EMPTY);
        }

        return self::$url_parts;
    }

    public static function GetPutVars()
    {
        if (self::$put_vars == null) {

            self::$put_vars = [];

            if (self::ServerStr('REQUEST_METHOD') == 'PUT') {

                parse_str(file_get_contents("php://input"), self::$put_vars);
            }
        }

        return self::$put_vars;
    }

    public static function PutStr($variable_name)
    {
        self::GetPutVars();

        if (isset(self::$put_vars[$variable_name])) {
            return filter_var(self::$put_vars[$variable_name], FILTER_UNSAFE_RAW);
        }

        return '';
    }

    public static function PutInt($variable_name)
    {
        self::GetPutVars();

        if (isset(self::$put_vars[$variable_name])) {
            return filter_var(self::$put_vars[$variable_name], FILTER_SANITIZE_NUMBER_INT);
        }

        return '';
    }

    public static function GetDeleteVars()
    {
        if (self::$delete_vars == null) {

            self::$delete_vars = [];

            if (self::ServerStr('REQUEST_METHOD') == 'DELETE') {

                parse_str(file_get_contents("php://input"), self::$delete_vars);
            }
        }

        return self::$delete_vars;
    }

    public static function DeleteStr($variable_name)
    {
        self::GetDeleteVars();

        if (isset(self::$delete_vars[$variable_name])) {
            return filter_var(self::$delete_vars[$variable_name], FILTER_UNSAFE_RAW);
        }

        return '';
    }

    public static function DeleteInt($variable_name)
    {
        self::GetDeleteVars();

        if (isset(self::$delete_vars[$variable_name])) {
            return filter_var(self::$delete_vars[$variable_name], FILTER_SANITIZE_NUMBER_INT);
        }

        return 0;
    }

    public static function UserIsBot()
    {
        $agent = strtolower(self::filterInput(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_UNSAFE_RAW));

        // Check if the user agent is empty
        if (empty($agent)) {
            // Most browsers identify themselves, so possibly a bot
            return true;
        }

        // Declare partial bot user agents (lowercase)
        $botUserAgents = array('bot', 'spider', 'slurp', 'jeeves', 'yahoo', 'yandex', 'bing', 'msn', 'crawl', 'google');

        // Check if a bot name is in the agent
        foreach ($botUserAgents as $botUserAgent) {
            if (stripos($agent, $botUserAgent) !== false) {
                // If it is, return true
                return true;
            }
        }

        // Probably a real user
        return false;
    }

    public static function IsHTTPS()
    {
        return (self::IsServerSet('HTTPS') && self::ServerStr('HTTPS') !== 'off');
    }

    public static function IsLocalhost()
    {
        return (self::IsServerSet('HTTP_HOST') && strpos(self::ServerStr('HTTP_HOST'), 'localhost') > -1);
    }

    public static function IsAjax()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    public static function IsAjaxPost()
    {
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
            return self::IsAjax();

        return false;
    }
}
