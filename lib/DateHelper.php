<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;


class DateHelper
{

    /**
     * Gets the current timezone offset as '-05:00' for use in MySQL timezone
     * @return false|string
     */
    static function GetTimezoneOffset()
    {
        return date('P');
    }

    /**
     * @return bool
     * @throws Exception
     */
	public static function IsDST() {

		// Check in New York
		$date = new DateTime('now', new DateTimeZone('America/New_York'));

		// offset in hours
		$offset =  $date->getOffset() / 3600;

		return $offset == -4;
	}

	/**
	 * Convert the value to a date
	 * @param mixed $value
	 * @return int|mixed
	 */
	private static function ValueToDate($value) {
		if (is_string($value))
			$dte = strtotime($value);
		else
			$dte = $value;
		return $dte;
	}

	/**
	 * Formats date and time for SQL
	 * @param string $datetime
	 * @return string Returns null if not successful.
	 */
	public static function DateTimeStrToSQL($datetime) {

		$val = self::ValueToDate($datetime);

		if (($val == null) || ($val === FALSE) || ($val == -1))
			return null;

		return date("Y-m-d H:i:s", $val);
	}

	/**
	 * Formats date for SQL
	 * @param string $date
	 * @return string Returns null if not successful.
	 */
	public static function DateStrToSQL($date) {

		$val = self::ValueToDate($date);

		if (($val == null) || ($val === FALSE) || ($val == -1))
			return null;

		return date("Y-m-d", $val);
	}

	/**
	 * Returns $value expressed in the US short date format. Returns a zero-length string if $value cannot be converted to a date.
	 * @param mixed $value
	 * @return string
	 */
	public static function ToShortDateString($value) {

		$dte = self::ValueToDate($value);

		if ($dte) {
			return date("n/d/Y", $dte);
		} else {
			return '';
		}
	}

	public static function ToShortDateTimeString($value) {

		$dte = self::ValueToDate($value);

		if ($dte) {
			return date("n/d/Y g:i A", $dte);
		} else {
			return '';
		}
	}

    /**
     * Returns something like 2002-02-02T14:22:22Z
     *
     * @param $value
     *
     * @return false|string|null
     */
    public static function ToISO8601Zulu($value)
    {
        $val = self::ValueToDate($value);

        if (($val == null) || ($val === FALSE) || ($val == -1))
            return null;

        return gmdate('Y-m-d\TH:i:s\Z', $val);
    }

    /**
     * Returns something like 2019-10-04T11:21:32+00:00
     *
     * @return string
     * @throws Exception
     */
    static function CurrentISOTimestamp()
    {
        $utc = new DateTime('now', new DateTimeZone('UTC'));
        return $utc->format(DateTime::ATOM);
    }

    /**
     * Returns $utc_date converted from UTC to EST (handles daylight-savings time correctly also).
     *
     * @param string $utc_date
     *
     * @return string
     * @throws Exception
     */
    static function UTC_to_EST($utc_date)
    {
        $tz = date_default_timezone_get();

        date_default_timezone_set('GMT');
        $d = new DateTime($utc_date);

        $d->setTimezone(new DateTimeZone('America/New_York'));

        // reset to the original time zone
        date_default_timezone_set($tz);

        return $d->format('Y-m-d  g:i:s A T');
    }

    /**
     * Returns negative if $date1 is greater than $date2
     *
     * @param int $date1
     * @param int $date2
     *
     * @return int
     * @throws Exception
     */
    static function DateDiff($date1, $date2)
    {
        if (!is_string($date1))
            $date1 = date('Y-m-d', $date1);
        if (!is_string($date2))
            $date2 = date('Y-m-d', $date2);

        $d1 = new DateTime($date1);
        $d2 = new DateTime($date2);

        /* @var $dateInterval DateInterval */
        $dateInterval = $d1->diff($d2);

        if ($dateInterval->invert)
            return $dateInterval->days * (-1);
        else
            return $dateInterval->days;
    }

    /**
     * Returns negative if $date1 is greater than $date2
     *
     * @param int $date1
     * @param int $date2
     *
     * @return int
     * @throws Exception
     */
    static function WeekDiff($date1, $date2)
    {
        $dif = self::DateDiff($date1, $date2);
        $dif = $dif / 7;

        if ($dif > 0)
            return floor($dif);
        else
            return ceil($dif);
    }

    /**
     *
     * @param mixed $date
     * @return false|int|mixed
     */
    static function FirstDayOfMonth($date)
    {
        $dte = self::ValueToDate($date);
        $dte = strtotime(date('m', $dte) . '/01/' . date('Y', $dte) . ' 00:00:00');
        return $dte;
    }

    /**
     *
     * @param mixed $date
     * @return false|int|mixed
     */
    static function LastDayOfMonth($date)
    {
        $dte = self::FirstDayOfMonth($date);
        $dte = strtotime('+1 month', $dte);
        $dte = strtotime('-1 second', $dte);
        return $dte;
    }

    /**
     * Returns the first day of the week containing $date.
     * Week starts on Sunday.
     * @param mixed $date
     * @return false|int|mixed
     */
    static function FirstDayOfWeek($date)
    {
        $dte = self::ValueToDate($date);
        while (date('w', $dte) != 0) {
            $dte = strtotime(date('Y-m-d', $dte) . ' -1 day');
        }
        return $dte;
    }

    /**
     * Returns the last day of the week containing $date.
     * Week ends on Saturday.
     * @param mixed $date
     * @return false|int|mixed
     */
    static function LastDayOfWeek($date)
    {
        $dte = self::ValueToDate($date);
        while (date('w', $dte) != 6) {
            $dte = strtotime(date('Y-m-d', $dte) . ' +1 day');
        }
        return $dte;
    }

    /**
     * Given the number of seconds since an event, such as the difference between 2 unix timestamps,
     * returns something like 5:12:34 or 05:12:34
     *
     * @param int $sec
     * @param bool $padHours
     *
     * @return string
     */
    static function Sec2HMS($sec, $padHours = false)
    {
        // start with a blank string
        $hms = '';

        // do the hours first: there are 3600 seconds in an hour, so if we divide
        // the total number of seconds by 3600 and throw away the remainder, we're
        // left with the number of hours in those seconds
        $hours = intval(intval($sec) / 3600);

        // add hours to $hms (with a leading 0 if asked for)
        $hms .= ($padHours) ? str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' : $hours . ':';

        // dividing the total seconds by 60 will give us the number of minutes
        // in total, but we're interested in *minutes past the hour* and to get
        // this, we have to divide by 60 again and then use the remainder
        $minutes = intval(($sec / 60) % 60);

        // add minutes to $hms (with a leading 0 if needed)
        $hms .= str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':';

        // seconds past the minute are found by dividing the total number of seconds
        // by 60 and using the remainder
        $seconds = intval($sec % 60);

        // add seconds to $hms (with a leading 0 if needed)
        $hms .= str_pad($seconds, 2, '0', STR_PAD_LEFT);

        // done!
        return $hms;
    }
}
