<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;


class Cookie {

    private $name;
    private $value;
    private $expire = 0;
    public $domain = '';
    private $has_changed = false;

    /**
     *
     * @param string $name
     * @param string $value
     * @param int $expire
     *
     * $expire = time() + number of seconds
     */
    public function __construct($name, $value='', $expire=0) {

        $this->name = $name;
        $this->value = $value;
        $this->expire = $expire;

        $this->has_changed = true;

        // domain must be empty for localhost
        $this->domain = self::GetDomain();
    }

    public function SetCookie() {
        if ($this->has_changed) {
            if (isset($_SERVER['SERVER_NAME'])) {

                /*
                 * NB: stopped using the built-in PHP function `setcookie` because it does not set the SameSite
                 *     attribute required by Chrome 80 (https://www.chromestatus.com/feature/5633521622188032).
                 *     The following is the text of the warning in the javascript console:
                 *     >> A cookie associated with a cross-site resource at http://test.rvwholesalers.com/ was set
                 *     >> without the `SameSite` attribute. A future release of Chrome will only deliver cookies with
                 *     >> cross-site requests if they are set with `SameSite=None` and `Secure`.
                 */

                $name = urlencode($this->name);
                $value = urlencode($this->value);
                $domain = urlencode($this->domain);

                $header = "Set-Cookie: {$name}={$value}; Path=/; Domain={$domain}; SameSite=Strict;";

                if ($this->expire != 0) {
                    $expires = date('D, d M Y H:i:s', $this->expire) . ' GMT';
                    $header .= " Expires={$expires};";
                }

                if (isset($_SERVER['HTTPS']))
                    $header .= ' Secure;';

                header($header);
            }
        }
    }

    public function DeleteCookie() {
        if (isset($_SERVER['SERVER_NAME'])) {
            setcookie($this->name, '', time() - 86400, '/', $this->domain);
        }
    }

    private static function GetDomain() {

        $return_val = '';

        // domain must be empty for localhost
        if (isset($_SERVER['SERVER_NAME'])) {
            $svr_name = $_SERVER['SERVER_NAME'];
            if (strtolower($svr_name) != 'localhost') {
                $parts = explode('.', $svr_name);

                // if there are 3 segments, use the root domain (2 segments)
                $part_count = count($parts);
                if ($part_count == 3) {
                    $return_val = '.' . $parts[1] . '.' . $parts[2];
                } else {
                    $return_val = '.' . $svr_name;
                }
            }
        }

        return $return_val;
    }

    public static function RemoveCookie($cookie_name) {
        setcookie($cookie_name, '', time() - 86400, '/', self::GetDomain());
    }

    /**
     * Returns the value of an existing cookie created on a previous page
     * @param string $cookie_name
     * @return Cookie
     */
    public static function GetExistingCookie($cookie_name) {

        $return_val = null;

        // get an existing cookie value
        if (isset($_COOKIE[$cookie_name])) {
            $return_val = new Cookie($cookie_name);
            $return_val->value = $_COOKIE[$cookie_name];
            $return_val->has_changed = false;
        }

        return $return_val;
    }

    public function Set_Value($value) {
        $this->value = $value;
    }

    public function Get_Value() {
        return $this->value;
    }

}
