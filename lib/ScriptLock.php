<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;

use Exception;

class ScriptLock
{
    private $temp_file;
    private $lock_file = false;
    private $disposed = false;

    public function __construct($file_path)
    {
        $this->temp_file = sys_get_temp_dir() . '/' . basename($file_path) . '.lock';
        $this->lock_file = fopen($this->temp_file, 'c');
        $got_lock = flock($this->lock_file, LOCK_EX | LOCK_NB, $would_block);
        if ($this->lock_file === false || (!$got_lock && !$would_block)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new Exception(
                'Unexpected error opening or locking lock file. Perhaps you don\'t  have permission to ' .
                'write to the lock file or its containing directory?'
            );
        }
        else if (!$got_lock && $would_block) {
            exit('Another instance is already running; terminating.' . PHP_EOL);
        }

        MiscFunctions::DebugPrint('Script lock obtained.');
    }

    public function __destruct()
    {
        if ($this->disposed === true)
            return;

        if ($this->lock_file !== false)
            $this->ReleaseLock();

        $this->disposed = true;
    }

    public function ReleaseLock()
    {
        if ($this->lock_file === false)
            return;

        ftruncate($this->lock_file, 0);
        flock($this->lock_file, LOCK_UN);

        if (is_file($this->temp_file))
            unlink($this->temp_file);

        $this->lock_file = false;

        MiscFunctions::DebugPrint('Script lock released.');
    }
}
