<?php /** @noinspection PhpUnused */


namespace Sibertec\Helpers;


class StringHelper
{

    /**
     * Determine if it is possible for the haystack to contain the needle
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    private static function may_contain($haystack, $needle)
    {
        $strlen = strlen($haystack);
        $test_len = strlen($needle);
        if ($strlen < 1) {
            return false;
        }
        if ($test_len < 1) {
            return false;
        }
        if ($test_len > $strlen) {
            return false;
        }

        return true;
    }

    public static function BeginsWith($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return (strpos($haystack, $needle) === 0);
    }

    /**
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public static function BeginsWithAny($haystack, $needles)
    {
        foreach($needles as $needle) {
            if (self::BeginsWith($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    public static function EndsWith($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return substr_compare($haystack, $needle, -(strlen($needle))) === 0;
    }

    public static function Contains($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return (strpos($haystack, $needle) !== false);
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR.
     *
     * @return string The path properly combined
     */
    public static function PathCombine()
    {
        $sep = DIRECTORY_SEPARATOR;
        $not_sep = ($sep == '/') ? '\\' : '/';

        $segments = func_get_args();
        $seg_cnt = count($segments);

        $return_val = '';

        // check for windows vs unix
        for ($i = 0; $i < $seg_cnt; $i++) {
            $segments[$i] = str_replace($not_sep, $sep, $segments[$i]);

            // remove trailing DIRECTORY_SEPARATOR from all segments
            while (self::EndsWith($segments[$i], $sep)) {
                $segments[$i] = substr($segments[$i], 0, strlen($segments[$i]) - 1);
            }

            // remove leading DIRECTORY_SEPARATOR from all segments except the first
            if ($i != 0) {
                while (self::BeginsWith($segments[$i], $sep)) {
                    $segments[$i] = substr($segments[$i], 1);
                }
            }

            // combine the segments
            if (!empty($segments[$i])) {

                if (empty($return_val)) {
                    $return_val = $segments[$i];
                } else {
                    $return_val .= $sep . $segments[$i];
                }
            }
        }

        return $return_val;
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR and converts to the absolute path
     *
     * @return string The canonical path, or false if path not found.
     */
    public static function RealPathCombine()
    {
        $args = func_get_args();
        $path = call_user_func_array('self::PathCombine', $args);
        return realpath($path);
    }

    /**
     * @return string The path properly combined
     */
    public static function UrlCombine()
    {
        $sep = '/';

        $segments = func_get_args();
        $seg_cnt = count($segments);

        $return_val = '';

        // check for windows vs unix
        for ($i = 0; $i < $seg_cnt; $i++) {

            // remove trailing SEPARATOR from all segments
            while (self::EndsWith($segments[$i], $sep)) {
                $segments[$i] = substr($segments[$i], 0, strlen($segments[$i]) - 1);
            }

            // remove leading DIRECTORY_SEPARATOR from all segments except the first
            if ($i != 0) {
                while (self::BeginsWith($segments[$i], $sep)) {
                    $segments[$i] = substr($segments[$i], 1);
                }
            }

            // combine the segments
            if (!empty($segments[$i])) {

                if (empty($return_val)) {
                    $return_val = $segments[$i];
                } else {
                    $return_val .= $sep . $segments[$i];
                }
            }
        }

        return $return_val;
    }

    public static function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            /** @noinspection PhpUndefinedFunctionInspection */
            /** @noinspection PhpComposerExtensionStubsInspection */
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function StripPhone($value, $trim_extra=false)
    {
        $regex = '/([^0-9])/';
        $stripped = preg_replace($regex, '', $value);

        // if the length is 11 and the number starts with 1, remove the first digit
        if ($trim_extra && (strlen($stripped) == 11) && substr($stripped, 0, 1) == '1')
            $stripped = substr($stripped, 1);

        return $stripped;
    }

    /**
     * Returns something like 937-843-9000 or (937) 843-9000
     *
     * @param $value
     * @param bool $use_parens
     *
     * @return string
     */
    public static function FormatPhone($value, $use_parens = true)
    {
        $stripped = self::StripPhone($value, true);

        switch (strlen($stripped)) {
            case 10:
                if ($use_parens)
                    return '(' . substr($stripped, 0, 3) . ') ' . substr($stripped, 3, 3) . '-' . substr($stripped, 6);
                else
                    return substr($stripped, 0, 3) . '-' . substr($stripped, 3, 3) . '-' . substr($stripped, 6);

            case 7:
                return substr($stripped, 0, 3) . '-' . substr($stripped, 3);
        }

        // if not able to format, return the original value
        return $value;
    }

    /**
     * Formats US phone numbers for SMS. Returns something like +19378439000
     *
     * @param $value
     * @param bool $add_plus
     *
     * @return false|string|string[]|null
     */
    public static function FormatPhoneSMS($value, $add_plus=true)
    {
        $stripped = self::StripPhone($value);

        switch (strlen($stripped)) {
            case 11:
                return ($add_plus ? '+' : '') . $stripped;

            case 10:
                return ($add_plus ? '+' : '') . '1' . $stripped;
        }

        // if not a full number, return what we have
        return $stripped;
    }

    /**
     * Formats a phone number for use in a regular expression. Returns something like '.*937.*843.*9000'
     *
     * @param $value
     *
     * @return string|null
     */
    public static function FormatPhoneRegex($value)
    {
        $stripped = self::StripPhone($value, true);

        if (strlen($stripped) == 10) {
            return '.*' . substr($stripped, 0, 3) . '.*' . substr($stripped, 3, 3) . '.*' . substr($stripped, 6);
        }

        // if not able to format, return null
        return null;
    }

	/**
	 * @param $value
	 *
	 * @return string
	 */
	public static function LowerEscape($value)
	{
		return urlencode(strtolower($value));
	}

    /**
     * Return the correct format of a word, singular or plural
     *
     * @param $quantity
     * @param $singular
     * @param null $plural
     *
     * @return string|null
     */
    public static function Pluralize($quantity, $singular, $plural=null)
    {
        if ($quantity == 1 || !strlen($singular)) return $singular;
        if ($plural !== null) return $plural;

        $last_letter = strtolower($singular[strlen($singular) - 1]);
        switch ($last_letter) {
            case 'y':
                return substr($singular, 0, -1) . 'ies';
            case 's':
                return $singular . 'es';
            default:
                return $singular . 's';
        }
    }
}
